<?php

Route::get('/addplayer', 'addplayer@show');

Route::post('/addplayer', 'addplayer@store');

Route::delete('/addplayer', 'addplayer@delete');

Route::get('/addscore', 'addscore@show');

Route::delete('/addscore', 'addscore@delete');

Route::post('/addscore', 'addscore@store');

Route::get('/leaderboard', 'leaderboard@show');

Route::delete('/leaderboard', 'leaderboard@delete');

Route::get('/player/{id}', 'playerHistory@show');

Route::get('/rules', function () {
    return view('rules');
});

