
@extends('layout.layout')

@section('content')
cv
    <div class="container-fluid">

        <h1>Record Score</h1>

        <form method="POST" action="addscore">
            {{ csrf_field() }}

            <div class="form-row">
                <div class="form-group col-lg-1">
                    <label for="console">Console #</label>
                    <input type="number" class="form-control" name="console" id="console" autofocus>
                </div>

                <div class="form-group col-lg-1">
                    <label for="place">Place</label>
                    <input type="number" class="form-control" id="place" name="place" placeholder="# Players Left">
                </div>

                <div class="form-group col-lg-1">
                    <label for="elims">Eliminations</label>
                    <input type="number" class="form-control" name="elims" id="elims">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        @if (count($errors))
            <br>
            <div class=" col-md-3 alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @include('tables/scorelist')
    </div>

@endsection