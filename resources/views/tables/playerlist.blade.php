<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <h3>Enrolled Players</h3>
            <table class="table text-center">
                <thead>
                <tr>
                    <th>Console</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($players as $player)
                        <tr>
                            <td><a href="/player/{{$player->console}}">{{$player->console}}</a></td>
                            <td>{{$player->name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>