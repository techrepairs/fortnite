<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <h3>Score History</h3>
            <table class="table text-center">
                <thead>
                <tr>
                    <th>Console</th>
                    <th>Place</th>
                    <th>Elims</th>
                    <th>Score</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($scores as $score)
                    <tr>
                        <td><a href="/player/{{$score->console}}">{{$score->console}}</a></td>
                        <td>{{$score->place}}</td>
                        <td>{{$score->elims}}</td>
                        <td>{{$score->score}}</td>
                        <td>
                            <form method="post" action="addscore">
                                {{ csrf_field() }}
                                @method("DELETE")
                                <input type="hidden" name="id" value="{{$score->id}}">
                                <button type="submit" class="btn btn-danger" value="delete">Delete Score</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>