
@extends('layout.layout')

@section('content')
<main role="main">

   {{-- <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Welcome to our first Fortnite Competition!</h1>
            <p class="lead text-muted">Here is how it works and our rules tonight</p>
            <p class="lead text-muted">Remember not to queue for the special game modes - only regular rules matches count</p>

        </div>
    </section>
    --}}
    <div class="album py-5 bg-light">
        <div class="container">
            <h2>Fortnite Competition Rules</h2>
            <h5 class="mb-3">Please queue for regular solo matches only, special modes don't count </h5>
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-4 box-shadow">
                        <div class="card-header">SCORING</div>
                        <div class="card-body">
                            <p class="card-text">Your Fortnite score today will be the total of your <strong>best</strong> 5 rounds of Fortnite. We only count your best 5 but you can submit as many scores as you have time for. The longer you outlast your opponents the higher your score! </p>
                            <div class="d-flex justify-content-between align-items-center">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-white bg-success mb-4 box-shadow">
                        <div class="card-header">BANKING YOUR SCORE</div>
                        <div class="card-body">
                            <p class="card-text">Your score won't count unless you raise your hand and wait for us to record your score. Make sure you stay on the spectator cam so we can see where you came!</p>
                            <div class="d-flex justify-content-between align-items-center">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-white bg-danger mb-4 box-shadow">
                        <div class="card-header">BONUS POINTS</div>
                        <div class="card-body">
                            <li>First Place - 20 Points </li>
                            <li>Top 5 - 15 Points </li>
                            <li>Top 10 - 10 Points </li>
                            <li>Top 25 - 5 Points </li>
                            <li>Eliminations - 5 Points</li>
                            <div class="d-flex justify-content-between align-items-center">
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                            <div class="col-md-4">
                              <div class="card text-white bg-info mb-4 box-shadow">
                                <div class="card-header">DUO Competitors</div>
                                <div class="card-body">
                                  <p class="card-text">If you are playing as a duo your only individual score counts towards your total. If your teammate falls - it is survival of the fittest! </p>
                                  <div class="d-flex justify-content-between align-items-center">
                                  </div>
                                </div>
                              </div>
                            </div>
                -->
                <div class="col-md-4">
                    <div class="card text-white bg-secondary mb-4 box-shadow">
                        <div class="card-header">TIME LIMIT</div>
                        <div class="card-body">
                            <p class="card-text">We aim to finish our games 2 hours from competition start, a staff member will keep you updated to how much time you have left</p>
                            <div class="d-flex justify-content-between align-items-center">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card bg-warning mb-4 box-shadow">
                        <div class="card-header">PRIZES!</div>
                        <div class="card-body">
                            <p class="card-text">The winner will win 1000 V-Bucks for Xbox or PS4. Second place has their choice from our minor prize cabinet. Third receives their choice of chips and a drink.</p>
                            <div class="d-flex justify-content-between align-items-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
@endsection