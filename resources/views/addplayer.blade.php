@extends('layout.layout')

@section('content')
    <div class="container-fluid">

        <h1>Add Player</h1>

        <form method="POST" action="addplayer">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="form-group col-lg-2">
                    <label for="console">Console #</label>
                    <input type="number" class="form-control" id="console" name="console" autofocus required>
                </div>
                <div class="form-group col-lg-2">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        @if (count($errors))
            <br>
            <div class=" col-md-3 alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @include('tables/playerlist')

            <div class="row">
                <div class="offset-1 col-lg-5">
                    <div class="mr-3">
                        <form method="post" action="addplayer">
                            {{ csrf_field() }}
                            @method("DELETE")
                            <button type="submit" class="btn btn-danger" value="delete">Delete All Players</button>
                        </form>
                    </div>
                    <form method="post" action="leaderboard">
                        {{ csrf_field() }}
                        @method("DELETE")
                        <button type="submit" class="btn btn-danger" value="delete">Reset Competition</button>
                    </form>
                </div>
            </div>

    </div>
@endsection