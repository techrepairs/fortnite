@extends('layout.layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>Console</th>
                    <th>Score</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($history as $score)
                    <tr>
                        <td>{{$score['console']}}</td>
                        <td>{{$score['score']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection