@extends('layout.layout')
@section('refresh',"20")
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Player</th>
                            <th>Score 1</th>
                            <th>Score 2</th>
                            <th>Score 3</th>
                            <th>Score 4</th>
                            <th>Score 5</th>
                            <th><strong>Total</strong></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                        @foreach ($scores as $score)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$score['name']}}</td>
                            <td>{{$score['game1']}}</td>
                            <td>{{$score['game2']}}</td>
                            <td>{{$score['game3']}}</td>
                            <td>{{$score['game4']}}</td>
                            <td>{{$score['game5']}}</td>
                            <td><strong>{{$score['total']}}</strong></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6">
                <h3>How your score is calculated</h3>
                <p>
                    Your score is the number of people you outlast + 10 points for each elimination + bonus points. 20 points for Victory Royale, 15 points for top 5, 10 points for top 10
                    and 5 points for top 20. For example the score for a person who came 15th with 2 eliminations would be 85 for the players they outlasted, 20 for the eliminations and 5 bonus
                    for top 20 for a total of 110 points.
                </p>
            </div>
        </div>
    </div>
@endsection