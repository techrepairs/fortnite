<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class score extends Model
{
    protected $fillable = ['console', 'place', 'elims', 'score'];
    public $timestamps = false;
    protected $table = 'scores';

}
