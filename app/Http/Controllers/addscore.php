<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Score;

class addscore extends Controller
{
    public function show()
    {
        $scores = Score::orderBy('id','desc')->get();
        $scores = $scores->filter(function ($value) {
            return $value->score > 0;
        });
        return view('addscore', compact('scores'));

    }


    public function store(Request $request)
    {
        $this->validate(request(), [
            'console' => 'required|integer|exists:players,console',
            'place' => 'required|integer',
            'elims' => 'required|integer',
        ]);

        $place = $request->place;
        $elims = $request->elims;

        if ($place == 1) {
            $bonus = 20;

        } elseif ($place <= 5 && $place > 1) {
            $bonus = 15;

        } elseif ($place <= 10 && $place > 5) {
            $bonus = 10;

        } elseif ($place <= 25 && $place > 10) {
            $bonus = 5;

        } else {
            $bonus = 0;
        }

        /* Bonus Points */
        $bonus = $bonus + 10 * $elims;
        $score = 100 - $place + $bonus;

        score::create([
            'console' => request('console'),
            'place' => request('place'),
            'elims' => request('elims'),
            'score' => $score
        ]);

        return redirect('/addscore');
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        Score::destroy($id);
        return redirect('/addscore');
    }
}

