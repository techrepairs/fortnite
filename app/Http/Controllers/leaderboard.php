<?php

namespace App\Http\Controllers;

use App\Player;
use App\Score;

class leaderboard extends Controller
{
    public function show()
    {
        $count = Player::all()->count();
        $collection = collect([]);

        for ($i = 1; $i <= $count; $i++) {
            $scorelist = Player::find($i)->scores;

            $name = Player::where('id', $i)->value('name');

            $top5 = $scorelist
                ->sortByDesc('score')
                ->pluck('score')
                ->take(5);
            //dd($top5);
            $total = $top5 ->sum();

            $collect = collect(['name' => $name, 'game1' => $top5[0], 'game2' => $top5[1],'game3' => $top5[2],'game4' => $top5[3],'game5' => $top5[4],'total'=>$total]);

            $collection -> prepend($collect);
        };

        $scores = $collection->sortByDesc('total');


        return view('leaderboard', compact('scores'));
    }

    public function delete()
    {
        Score::truncate();
        Player::truncate();
        return redirect('/leaderboard');
    }
}