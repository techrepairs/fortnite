<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Player;
use App\Score;
class addplayer extends Controller
{
    public function show()
    {
        $players = Player::all();

        return view('addplayer', compact('players'));
    }


    public function store()
    {
        $this->validate(request(), [
            'console' => 'required|unique:players,console|string|max:5',
            'name' => 'required|unique:players,name|max:15',
        ]);

        Player::create([
            'console' => request('console'),
            'name' => request('name')
        ]);

        for ($i = 1; $i <=5; $i++) {
            score::create([
                'console' => request('console'),
                'place' => 0,
                'elims' => 0,
                'score' => 0
            ]);
        }

        return redirect('addplayer');
    }

    public function delete()
    {
        Player::truncate();
        return redirect('/addplayer');
    }

}