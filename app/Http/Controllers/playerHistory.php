<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Score;

class playerHistory extends Controller
{
    public function show($id)
    {
        $history = Score::where('console',$id)->get();
        $history = $history->filter(function ($value) {
            return $value->score > 0;
        });
        return view('playerHistory', compact('history'));
    }
}
