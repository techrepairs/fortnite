<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['console', 'name'];
    public $timestamps = false;
    protected $table = 'players';
    public function scores()
    {
        return $this->hasMany('App\Score', 'console', 'console');
    }
}